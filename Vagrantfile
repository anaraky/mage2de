# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

mv = ""
sd = ""
if ARGV[0] == 'up' && (Dir.glob("#{File.dirname(__FILE__)}/.vagrant/machines/default/virtualbox/*").empty? || ARGV[1] == '--provision')
  print "1) Magento 2.1.x CE\n2) Magneto 2.0.x CE\nWhich version do you need? [1-2]: "
  input = Integer(STDIN.gets)
  print "\n"
  if input == 1
    mvs = ["2.1.6", "2.1.5", "2.1.4", "2.1.3", "2.1.2", "2.1.1", "2.1.0"]
  elsif input == 2
    mvs = ["2.0.13", "2.0.12", "2.0.11", "2.0.10", "2.0.9", "2.0.8", "2.0.7", "2.0.6", "2.0.5", "2.0.4", "2.0.2"]
  else
    print "Your choice is out of range!\n"
    exit(-1)
  end

  i  = 0
  for v in mvs
    i += 1
    print(i, ") ", v, "\n");
  end
  print("Please select the exact version [1-", mvs.length, "]: ")
  input = Integer(STDIN.gets)
  if input > 0 && input <= mvs.length
    mv = mvs[input - 1]
  else
    print "Your choice is out of range!\n"
    exit(-1)
  end
  
  print "Do you need the sample data? [y/n]: "
  input = STDIN.gets.chomp
  if input == 'y' || input == 'Y'
    sd = "_sample_data"
  end
  print "\n"
end

Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "paliarush/magento2.ubuntu"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.69.69"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "./src/", "/var/www/l.dev/web/", type:"rsync", create: true, rsync__args:["--verbose", "--archive", "-z", "--copy-links"] 

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
     vb.cpus = "2"
     vb.memory = "4096"
  end

  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  #config.vm.provision :shell, :path => "install.sh", :args => [mv, sdv]
  config.vm.provision "shell", args: [mv, sd], inline: <<-SHELL
    #!/bin/bash
    MV=$1; # MAGENTO VERSION
    SD=$2;

    SOURCE_BASE_PATH="https://bitbucket.org/anaraky/mage2de/downloads/";

    apt-get install unzip;
    mkdir -p "/var/www/l.dev/web/pub";
    mkdir -p "/var/www/l.dev/logs";
    echo -e "<VirtualHost *:80>\n  ServerName l.dev\n  ServerAlias www.l.dev\n  ServerAdmin webmaster@localhost\n  DocumentRoot /var/www/l.dev/web\n  <Directory /var/www/l.dev/web/>\n    DirectoryIndex index.php\n    Options Indexes FollowSymLinks MultiViews\n    AllowOverride All\n    Order allow,deny\n    allow from all\n  </Directory>\n  ErrorLog /var/www/l.dev/logs/error.log\n  CustomLog /var/www/l.dev/logs/access.log combined\n</VirtualHost>" > /etc/apache2/sites-available/l.dev.conf;
    chmod -R 0777 "/etc/apache2/sites-available/l.dev.conf";
    ln -s /etc/apache2/sites-available/l.dev.conf /etc/apache2/sites-enabled/l.dev.conf;
    cd /var/www/l.dev/web/;
    echo "--------------- Adminer -----------------";
    wget --progress=bar:force https://github.com/vrana/adminer/releases/download/v4.2.5/adminer-4.2.5-mysql-en.php;
    mv ./adminer-4.2.5-mysql-en.php ./pub/adm.php;
    echo "--------------------------------------------------";
    mysql -u root -e "CREATE DATABASE IF NOT EXISTS magento;";

    echo "--------------- Magento -----------------";
    wget --progress=bar:force ${SOURCE_BASE_PATH}Magento-CE-${MV}${SD}.zip;
    unzip -q Magento-CE-${MV}${SD}.zip;
    rm -rf ./Magento-CE-${MV}${SD}.zip;
    echo "--------------------------------------------------";

    chown vagrant:vagrant -R /var/www/l.dev/;
    chmod -R 0777 "/var/www/l.dev";
    service apache2 restart;
  SHELL
end
